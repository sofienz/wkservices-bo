const router = require("express").Router();
const register = require("../operations/user/register");
const user = require("../operations/user/user");
const { verifyToken } = require("../services/Utils/verifyToken");

router.post("/register", async (req, res) => {
  try {
    const response = await register.register(req.body);
    res.status(response.code).send(response);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
});

router.post("/login", async (req, res) => {
  try {
    const response = await user.login(req.body);
    res.status(response.code).send(response);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
});
router.post("/renewToken", async (req, res) => {
  const refreshToken = req.body.token;
  try {
    const response = await user.renewToken(refreshToken);
    res.status(response.code).send(response);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
});

router.post("/logout", verifyToken, async (req, res) => {
  try {
    const response = await user.logout(req.user.id);
    res.status(response.code).send(response);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
});

module.exports = router;
