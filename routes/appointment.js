const router = require("express").Router();
const Appointment = require("../operations/appointment");
const { verifyToken } = require("../services/Utils/verifyToken");
const upload = require("../middlewares/upload");
router.post("/upload", upload.single("file"), async (req, res) => {
  try {
    const response = await Appointment.upload(req);
    res.status(response.code).send(response);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
});

router.post("/notify", async (req, res) => {
  try {
    const response = await Appointment.notify(req.body);
    res.status(response.code).send(response);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
});
router.get("/list", async (req, res) => {
  try {
    const response = await Appointment.getAllAppointments();
    res.status(response.code).send(response);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
});
router.post("/findByFilters", async (req, res) => {
  try {
    const response = await Appointment.getAppointmentsByFilter(req.body);
    res.status(response.code).send(response);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
});
router.post("/countByFilters", async (req, res) => {
  try {
    const response = await Appointment.countAppointmentsByFilter(req.body);
    res.status(response.code).send(response);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
});
router.get("/:id", async (req, res) => {
  try {
    const response = await Appointment.getAppointmentsById(req.params.id);
    res.status(response.code).send(response);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
});
router.post("/update/:id", async (req, res) => {
  try {
    const response = await Appointment.updateAppointment(
      req.params.id,
      req.body
    );
    res.status(response.code).send(response);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
});
router.post("/approve/:id", async (req, res) => {
  try {
    const response = await Appointment.approveAppointment(
      req.params.id,
      req.body
    );
    res.status(response.code).send(response);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
});
router.post("/confirm/:id", async (req, res) => {
  try {
    const response = await Appointment.confirmAppointment(
      req.params.id,
      req.body
    );
    res.status(response.code).send(response);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
});

module.exports = router;
