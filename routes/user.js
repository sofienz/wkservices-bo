const router = require("express").Router();
const register = require("../operations/user/register");
const user = require("../operations/user/user");
const {
  verifyToken,
  verifyTokenAndAdmin,
} = require("../services/Utils/verifyToken");

router.post("/create", verifyTokenAndAdmin, async (req, res) => {
  try {
    const response = await register.register(req.body);
    res.status(response.code).send(response);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
});
router.post("/updateStatus/:id", verifyTokenAndAdmin, async (req, res) => {
  try {
    const response = await user.updateUserStatus(
      req.params.id,
      req.body.status
    );
    res.status(response.code).send(response);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
});

router.get("/list", verifyTokenAndAdmin, async (req, res) => {
  try {
    const response = await user.getAllUsers();
    res.status(response.code).send(response);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
});
router.post("/findByFilters", verifyToken, async (req, res) => {
  try {
    const response = await user.getUsersByFilter(req.body);
    res.status(response.code).send(response);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
});

module.exports = router;
