var express = require("express");
const cors = require("cors");
var path = require("path");
var logger = require("morgan");
var cookieParser = require("cookie-parser");
const dotenv = require("dotenv");
var app = express();
app.disable("x-powered-by");
const mongoose = require("mongoose");
const config = require("./config/config.js");
const authRoute = require("./routes/auth");
const usersRoute = require("./routes/user");
const appointementRoute = require("./routes/appointment");
global.__basedir = __dirname + "/server" + "/..";
const corsOptions = {
  origin: ["*"],
  allowedHeaders: [
    "Content-Type",
    "Authorization",
    "Access-Control-Allow-Methods",
    "Access-Control-Request-Headers",
  ],
  credentials: true,
  enablePreflight: true,
};
app.use(function (req, res, next) {
  //set headers to allow cross origin request.
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
dotenv.config();
app.use(cors(corsOptions));
app.options("*", cors(corsOptions));

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.static("public"));
app.use(express.json({ limit: "50mb", extended: true }));
app.use(express.urlencoded({ limit: "50mb", extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

//MONGO SETTINGS
const mongoUrl =
  process.env.NODE_ENV === "testing"
    ? process.env.MONGO_URL_TEST
    : process.env.MONGO_URL;
mongoose
  .connect(mongoUrl)
  .then(() => console.log("DB Connection Successfull!"))
  .catch((err) => {
    console.log(err);
  });

// END MONGO

//API SETTINGS
app.use("/api/auth", authRoute);
app.use("/api/users", usersRoute);
app.use("/api/appointment", appointementRoute);

//

module.exports = app;
