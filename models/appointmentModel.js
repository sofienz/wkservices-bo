const mongoose = require("mongoose");

const { stringify } = require("jade/lib/utils");

const userSchema = new mongoose.Schema(
  {
    consignee: {
      type: String,
    },
    phone: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    coi_needed: {
      type: Boolean,
      default: false,
    },
    coi_template: {
      type: String,
    },
    coi_document_url: {
      type: String,
    },
    city: {
      type: String,
      required: true,
    },
    quantity: {
      type: Number,
      required: true,
    },
    order: {
      type: Number,
      required: true,
    },
    service: {
      type: String,
      enum: ["STD", "WG"],
      default: "STD",
    },
    appointmentDate: {
      type: String,
    },
    proposedAppointmentDate: {
      type: [String],
    },
    appointmentTime: {
      type: String,
    },
    address: {
      type: String,
    },
    notes: {
      type: String,
    },
    status: {
      type: String,
      enum: ["CREATED", "NOTIFIED", "CONFIRMED", "PENDING_APPROVAL"],
      default: "CREATED",
    },
    serviceChangeRequest: {
      type: Boolean,
      default: false,
    },
    timeChangeRequest: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

const user = mongoose.model("Appointment", userSchema);
module.exports = user;
