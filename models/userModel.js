const mongoose = require("mongoose");
var bcrypt = require("bcryptjs");
const { stringify } = require("jade/lib/utils");

const userSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true,
    },
    fullName: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
      select: false,
    },
    warehouse: {
      type: mongoose.Schema.ObjectId,
      ref: "Warehouse",
    },
    role: {
      type: String,
      enum: ["ADMIN", "MANAGER", "STOREKEEPER", "MECHANIC", "DELIVERYMAN"],
      default: "USER",
    },
    status: {
      type: String,
      enum: ["ACTIVE", "BLOCKED"],
      default: "ACTIVE",
    },
    assignedToScooter: Boolean,
    access_token: String,
    refresh_token: String,
    resetPasswordToken: String,
    resetPasswordExpiry: Date,
  },
  { timestamps: true }
);
userSchema.methods.comparePassword = function (candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};
const user = mongoose.model("User", userSchema);
module.exports = user;
