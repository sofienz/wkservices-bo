module.exports = {
  createResponse({ status, message, result, code }) {
    return {
      status: status || "OK",
      message: message || null,
      result: result || null,
      code: code ? code : 200,
    };
  },
};
