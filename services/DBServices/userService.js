const User = require("../../models/userModel");
module.exports = new (class UserDbService {
  constructor() {
    this.userModel = User;
  }
  getById(id) {
    return this.userModel.findById(id).lean();
  }
  async getByEmail(email, isPasswordNeeded) {
    if (isPasswordNeeded) {
      return await this.userModel.findOne({ email }).select("+password").lean();
    } else {
      return await this.userModel.findOne({ email }).lean();
    }
  }

  getByToken(token) {
    return this.userModel.findOne({ access_token: token }).lean();
  }
  getByRefreshToken(refreshToken) {
    return this.userModel.findOne({ refresh_token: refreshToken }).lean();
  }
  updateToken(user) {
    return this.userModel
      .findOneAndUpdate(
        {
          _id: user._id,
        },
        {
          access_token: user.access_token,
          refresh_token: user.refresh_token,
        },
        {
          new: true,
        }
      )
      .lean();
  }
  updateStatus(id, status) {
    return this.userModel
      .findOneAndUpdate(
        {
          _id: id,
        },
        {
          status: status,
        },
        {
          new: true,
        }
      )
      .lean();
  }
  updateIfAssignedToScooter(id, isAssignedToScooter) {
    return this.userModel
      .findOneAndUpdate(
        {
          _id: id,
        },
        {
          assignedToScooter: isAssignedToScooter,
        },
        {
          new: true,
        }
      )
      .lean();
  }

  updateUser(id, user) {
    return this.userModel
      .findByIdAndUpdate(id, { $set: user }, { new: true })
      .lean();
  }
  addUser(user) {
    return this.userModel.create(user);
  }
  async getAllUsers() {
    return await this.userModel.find();
  }
  async getUsersByFilter(filter) {
    return await this.userModel.find(filter);
  }
})();
