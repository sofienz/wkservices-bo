const Appointment = require("../../models/appointmentModel");
module.exports = new (class WarehouseDbService {
  constructor() {
    this.appointmentModel = Appointment;
  }

  createAppointment(warehouse) {
    return this.appointmentModel.create(warehouse);
  }
  createAppointments(appointmentList) {
    return this.appointmentModel.insertMany(appointmentList);
  }
  async updateAppointment(id, apointment) {
    return await this.appointmentModel
      .findOneAndUpdate({ _id: id }, apointment, { new: true })
      .lean();
  }
  async getAllAppointments() {
    return await this.appointmentModel.find();
  }
  async findByFilters(filters) {
    return await this.appointmentModel.find(filters);
  }
  async findById(id) {
    return await this.appointmentModel.findById(id);
  }
  async findByIdWithoutFields(id) {
    return await this.appointmentModel
      .findById(id)
      .select(["-_id", "-__v", "-createdAt", "-updatedAt"]);
  }
  async countByFilters(filters) {
    return await this.appointmentModel.count(filters);
  }
})();
