const Utils = require("../../services/Utils/utils");
const UserService = require("../../services/DBServices/userService");
var Bcrypt = require("bcryptjs");
var EmailValidator = require("email-validator");

module.exports = new (class registerOperation {
  constructor() {
    this.service = UserService;
    this.utils = Utils;
  }
  async register(userToRegister) {
    try {
      var user = {};
      let isEmail = this.validateEmail(userToRegister.email);
      if (isEmail) {
        user = await this.service.getByEmail(userToRegister.email, false);
      } else {
        return this.utils.createResponse({
          message: "incorrect_email",
          status: "ERROR",
        });
      }
      if (user) {
        return this.utils.createResponse({
          message: "email_exists",
          status: "ERROR",
        });
      } else {
        var hashedPassword = Bcrypt.hashSync(userToRegister.password, 8);
        userToRegister.password = hashedPassword;
        const newUser = await this.service.addUser(userToRegister);
        const { password, ...others } = newUser._doc;
        return this.utils.createResponse({ result: { ...others } });
      }
    } catch (error) {
      return this.utils.createResponse({
        code: 500,
        message: error.message,
        status: "ERROR",
      });
    }
  }
  validateEmail(email) {
    return EmailValidator.validate(String(email).toLowerCase());
  }
})();
