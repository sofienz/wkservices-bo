const Utils = require("../../services/Utils/utils");
const UserService = require("../../services/DBServices/userService");
var Bcrypt = require("bcryptjs");
var Jwt = require("jsonwebtoken");
var EmailValidator = require("email-validator");

module.exports = new (class userOperation {
  constructor() {
    this.service = UserService;
    this.utils = Utils;
    this.bcrypt = Bcrypt;
    this.jwt = Jwt;
  }
  async login({ login, password, udid }) {
    try {
      var user = {};

      user = await this.service.getByEmail(login, true);

      if (user) {
        if (!this.bcrypt.compareSync(password, user.password)) {
          return this.utils.createResponse({
            message: "wrong_password",
            status: "ERROR",
          });
        } else {
          if (user.status === "ACTIVE") {
            var token = this.generateAccessToken(user._id, user.role);
            var refreshToken = this.jwt.sign(
              { id: user._id },
              process.env.refreshSecret
            );
            user.access_token = token;
            user.refresh_token = refreshToken;
            let userRender = await this.service.updateToken(user);
            return this.utils.createResponse({ result: userRender });
          } else {
            return this.utils.createResponse({
              error: true,
              message: user.status,
              code: 401,
            });
          }
        }
      } else {
        return this.utils.createResponse({
          status: "ERROR",
          message: "NOT_FOUND",
        });
      }
    } catch (error) {
      return this.utils.createResponse({
        code: 500,
        message: error.message,
        status: "ERROR",
      });
    }
  }
  async getAllUsers() {
    try {
      const usersList = await this.service.getAllUsers();
      return this.utils.createResponse({ result: usersList });
    } catch (error) {
      return this.utils.createResponse({
        code: 500,
        message: error.message,
        status: "ERROR",
      });
    }
  }
  async getUsersByFilter(filter) {
    try {
      const usersList = await this.service.getUsersByFilter(filter);
      return this.utils.createResponse({ result: usersList });
    } catch (error) {
      return this.utils.createResponse({
        code: 500,
        message: error.message,
        status: "ERROR",
      });
    }
  }
  async updateUserStatus(id, status) {
    try {
      const updatedUser = await this.service.updateStatus(id, status);
      return this.utils.createResponse({ result: updatedUser });
    } catch (error) {
      return this.utils.createResponse({
        code: 500,
        message: error.message,
        status: "ERROR",
      });
    }
  }
  async updateUser(id, user) {
    try {
      const updatedUser = await this.service.updateUser(id, user);
      return this.utils.createResponse({ result: updatedUser });
    } catch (error) {
      return this.utils.createResponse({
        code: 500,
        message: error.message,
        status: "ERROR",
      });
    }
  }
  forgotPassword({ email }) {
    try {
      if (String(email) == "admin") {
      } else {
      }
    } catch (error) {
      return this.utils.createResponse({
        code: 500,
        message: error.message,
        status: "ERROR",
      });
    }
  }
  async renewToken(refreshToken) {
    if (refreshToken == null)
      return this.utils.createResponse({
        error: true,
        message: "Invalid request",
        code: 401,
      });
    const user = await this.service.getByRefreshToken(refreshToken);
    if (user == null) {
      return this.utils.createResponse({
        message: "Refresh Token is not valid!",
        code: 403,
      });
    }
    return this.jwt.verify(
      refreshToken,
      process.env.refreshSecret,
      (err, user) => {
        if (err)
          return this.utils.createResponse({
            error: true,
            message: "Refresh Token is not valid!",
            code: 403,
          });
        const accessToken = this.generateAccessToken(user.id);
        return this.utils.createResponse({
          result: accessToken,
        });
      }
    );
  }
  async logout(userId) {
    try {
      let user = {
        _id: userId,
        access_token: "",
        refresh_token: "",
      };
      await this.service.updateToken(user);
      return this.utils.createResponse({ message: "logout successful" });
    } catch (error) {
      return this.utils.createResponse({
        code: 500,
        message: error.message,
        status: "ERROR",
      });
    }
  }
  generateAccessToken(id, role) {
    return this.jwt.sign({ id: id, role: role }, process.env.secret, {
      expiresIn: "5h",
    });
  }

  validateEmail(email) {
    return EmailValidator.validate(String(email).toLowerCase());
  }
})();
