const Utils = require("../services/Utils/utils");
const messageBird = require("../services/Utils/messageBird");
const nodeMailerHelper = require("../services/Utils/nodeMailerHelper");
const readXlsxFile = require("read-excel-file/node");
const AppointmentService = require("../services/DBServices/appointmentService");

module.exports = new (class AppointmentOperation {
  constructor() {
    this.utils = Utils;
    this.messageBird = messageBird;
    this.nodeMailerHelper = nodeMailerHelper;
    this.appointmentService = AppointmentService;
  }

  async notify(appointments) {
    let numbersList = [];

    appointments.forEach(async (appointment) => {
      let numbersList = [];
      numbersList.push(appointment.phone);
      try {
        await this.messageBird.sendSMS(
          numbersList,
          "hello " + appointment.consignee
        );
        await this.nodeMailerHelper.sendEMail(
          appointment.email,
          appointment.consignee
        );
        appointment.status = "NOTIFIED";
        await this.appointmentService.updateAppointment(
          appointment._id,
          appointment
        );
      } catch (error) {
        return this.utils.createResponse({
          code: 500,
          message: error.message,
          status: "ERROR",
        });
      }
    });
    return this.utils.createResponse({
      status: "OK",
    });
  }
  async updateAppointment(id, data) {
    try {
      const appointmentUpdated =
        await this.appointmentService.updateAppointment(id, data);
      return this.utils.createResponse({ result: appointmentUpdated });
    } catch (error) {
      return this.utils.createResponse({
        code: 500,
        message: error.message,
        status: "ERROR",
      });
    }
  }
  async approveAppointment(id, data) {
    try {
      const appointment = await this.appointmentService.findById(id);
      const dataToUpdate = {};
      if (appointment.timeChangeRequest) {
        dataToUpdate["appointmentDate"] = data.appointmentDate;
        dataToUpdate["timeChangeRequest"] = false;
      }
      if (appointment.serviceChangeRequest) {
        if (data.serviceChangeResult) {
          dataToUpdate["service"] = "WGS";
        }
        dataToUpdate["serviceChangeRequest"] = false;
      }

      dataToUpdate["status"] = "CONFIRMED";
      const appointmentUpdated =
        await this.appointmentService.updateAppointment(id, dataToUpdate);

      await this.nodeMailerHelper.sendAppointmentConfirmationEMail(
        appointment.email,
        appointment.consignee
      );
      return this.utils.createResponse({ result: appointmentUpdated });
    } catch (error) {
      return this.utils.createResponse({
        code: 500,
        message: error.message,
        status: "ERROR",
      });
    }
  }
  async confirmAppointment(id, data) {
    try {
      if (data.timeChangeRequest || data.serviceChangeRequest) {
        data["status"] = "PENDING_APPROVAL";
      } else {
        data["status"] = "CONFIRMED";
      }

      const appointmentUpdated =
        await this.appointmentService.updateAppointment(id, data);
      // send sms et mail
      await this.nodeMailerHelper.sendAppointmentConfirmationEMail(
        data.email,
        data.consignee
      );
      return this.utils.createResponse({ result: appointmentUpdated });
    } catch (error) {
      return this.utils.createResponse({
        code: 500,
        message: error.message,
        status: "ERROR",
      });
    }
  }

  async getAllAppointments() {
    try {
      const appointmentsList =
        await this.appointmentService.getAllAppointments();
      return this.utils.createResponse({ result: appointmentsList });
    } catch (error) {
      return this.utils.createResponse({
        code: 500,
        message: error.message,
        status: "ERROR",
      });
    }
  }
  async getAppointmentsByFilter(filter) {
    try {
      const appointmentsList = await this.appointmentService.findByFilters(
        filter
      );
      return this.utils.createResponse({ result: appointmentsList });
    } catch (error) {
      return this.utils.createResponse({
        code: 500,
        message: error.message,
        status: "ERROR",
      });
    }
  }
  async countAppointmentsByFilter(filter) {
    try {
      const appointmentsList = await this.appointmentService.countByFilters(
        filter
      );
      return this.utils.createResponse({ result: appointmentsList });
    } catch (error) {
      return this.utils.createResponse({
        code: 500,
        message: error.message,
        status: "ERROR",
      });
    }
  }
  async getAppointmentsById(id) {
    try {
      const appointment = await this.appointmentService.findByIdWithoutFields(
        id
      );

      return this.utils.createResponse({ result: appointment });
    } catch (error) {
      return this.utils.createResponse({
        code: 500,
        message: error.message,
        status: "ERROR",
      });
    }
  }

  async upload(req) {
    try {
      if (req.file == undefined) {
        return this.utils.createResponse({
          code: 500,
          message: "Please upload an excel file!",
          status: "ERROR",
        });
      }
      let path =
        __basedir + "/resources/static/assets/uploads/" + req.file.filename;
      let appointments = [];
      await readXlsxFile(path).then((rows) => {
        // skip header
        rows.shift();

        rows.forEach((row) => {
          let appointment = {
            order: row[0],
            quantity: row[1],
            consignee: row[2],
            phone: row[3],
            email: row[4],
            address: row[5],
            city: row[6],
            service: row[7],
          };
          appointments.push(appointment);
        });
      });
      const appointmentsCreated =
        await this.appointmentService.createAppointments(appointments);
      return this.utils.createResponse({ result: appointmentsCreated });
    } catch (error) {
      console.log(error);
      return this.utils.createResponse({
        code: 500,
        message: error.message,
        status: "ERROR",
      });
    }
  }
})();
